SELECT
	 t.[EventDateTime]      AS [EventDateTime]
	,SUM(t.[num_of_reads])  AS [num_of_reads]
	,SUM(t.[num_of_writes]) AS [num_of_writes]
FROM
	[dwh_InstanceIOStatus] t
WHERE
	t.[InstanceConnectionId] = $_id_connection
	AND t.[EventDateTime] > (
		SELECT
			DATETIME(MAX(t.[EventDateTime]), "-1 day")
		FROM
			[dwh_InstanceIOStatus] t
		WHERE
			t.[InstanceConnectionId] = $_id_connection
	)
GROUP BY
	t.[EventDateTime]
;
