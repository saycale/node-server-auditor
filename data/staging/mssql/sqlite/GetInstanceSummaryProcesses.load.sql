SELECT
	 t.[EventDateTime]               AS [EventDateTime]
	,t.[NumberOfSystemProcesses]     AS [NumberOfSystemProcesses]
	,t.[NumberOfUserProcesses]       AS [NumberOfUserProcesses]
	,t.[NumberOfActiveUserProcesses] AS [NumberOfActiveUserProcesses]
	,t.[NumberOfBlockedProcesses]    AS [NumberOfBlockedProcesses]
FROM
	[dwh_InstanceSummaryProcesses] t
WHERE
	t.[InstanceConnectionId] = $_id_connection
	AND t.[EventDateTime] > (
		SELECT
			DATETIME(MAX(t.[EventDateTime]), "-1 day")
		FROM
			[dwh_InstanceSummaryProcesses] t
		WHERE
			t.[InstanceConnectionId] = $_id_connection
	)
ORDER BY
	t.[EventDateTime] DESC
;
