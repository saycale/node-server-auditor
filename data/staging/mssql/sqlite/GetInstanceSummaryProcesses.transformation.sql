INSERT INTO [dwh_InstanceSummaryProcesses]
(
	 [EventDateTime]
	,[InstanceConnectionId]
	,[NumberOfSystemProcesses]
	,[NumberOfUserProcesses]
	,[NumberOfActiveUserProcesses]
	,[NumberOfBlockedProcesses]
)
SELECT
	 t.[EventDateTime]
	,t.[_id_connection]
	,t.[NumberOfSystemProcesses]
	,t.[NumberOfUserProcesses]
	,t.[NumberOfActiveUserProcesses]
	,t.[NumberOfBlockedProcesses]
FROM
	[${GetInstanceSummaryProcesses}{0}$] t
	LEFT OUTER JOIN [dwh_InstanceSummaryProcesses] dISP ON
		dISP.[EventDateTime] = t.[EventDateTime]
		AND dISP.[InstanceConnectionId] = t.[_id_connection]
WHERE
	t.[_id_connection] = $_id_connection
	AND dISP.[InstanceConnectionId] IS NULL
;
