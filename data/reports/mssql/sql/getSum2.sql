SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

SELECT
	 @@SERVERNAME AS [HostName]
	,1            AS [RecordSet]
	,GETDATE()    AS [CurrentTime]
	,1 + 1        AS [Solution]
;

SELECT
	 @@SERVERNAME AS [HostName]
	,2            AS [RecordSet]
	,GETDATE()    AS [CurrentTime]
	,2 + 2        AS [Solution]
;

SELECT
	 @@SERVERNAME AS [HostName]
	,3            AS [RecordSet]
	,GETDATE()    AS [CurrentTime]
	,3 + 3        AS [Solution]
;
