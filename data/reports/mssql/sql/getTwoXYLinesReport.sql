SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

DECLARE
	@intIndex [INTEGER]
;

CREATE TABLE #t1
(
	 [idRecordSet] [INTEGER]        NOT NULL
	,[idLine]      [INTEGER]        NOT NULL
	,[idPoint]     [INTEGER]        NOT NULL
	,[x]           [DECIMAL](24, 4) NOT NULL
	,[y]           [DECIMAL](24, 4) NOT NULL
);

SET @intIndex = 1;

WHILE (@intIndex <= 5)
BEGIN
	------------------------------------------------------------------------------------------------
	-- recordset: 1
	------------------------------------------------------------------------------------------------
	INSERT INTO #t1
	(
		 [idRecordSet]
		,[idLine]
		,[idPoint]
		,[x]
		,[y]
	)
	VALUES
	(
		 1
		,1
		,@intIndex
		,-5.0  + 10.0 * RAND()
		,-10.0 + 20.0 * RAND()
	);

	------------------------------------------------------------------------------------------------
	-- recordset: 2
	------------------------------------------------------------------------------------------------
	INSERT INTO #t1
	(
		 [idRecordSet]
		,[idLine]
		,[idPoint]
		,[x]
		,[y]
	)
	VALUES
	(
		 2
		,1
		,@intIndex
		,-2.0  + 15.0 * RAND()
		,-4.0  + 25.0 * RAND()
	);

	INSERT INTO #t1
	(
		 [idRecordSet]
		,[idLine]
		,[idPoint]
		,[x]
		,[y]
	)
	VALUES
	(
		 2
		,2
		,@intIndex
		,-5.0  + 10.0 * RAND()
		,-10.0 + 20.0 * RAND()
	);

	------------------------------------------------------------------------------------------------
	-- recordset: 3
	------------------------------------------------------------------------------------------------
	INSERT INTO #t1
	(
		 [idRecordSet]
		,[idLine]
		,[idPoint]
		,[x]
		,[y]
	)
	VALUES
	(
		 3
		,1
		,@intIndex
		,-2.0  + 11.0 * RAND()
		,-4.0  + 35.0 * RAND()
	);

	INSERT INTO #t1
	(
		 [idRecordSet]
		,[idLine]
		,[idPoint]
		,[x]
		,[y]
	)
	VALUES
	(
		 3
		,2
		,@intIndex
		,-2.0  + 10.0 * RAND()
		,-4.0  + 29.0 * RAND()
	);

	INSERT INTO #t1
	(
		 [idRecordSet]
		,[idLine]
		,[idPoint]
		,[x]
		,[y]
	)
	VALUES
	(
		 3
		,3
		,@intIndex
		,-2.0  + 18.0 * RAND()
		,-4.0  + 23.0 * RAND()
	);

	SET @intIndex = @intIndex + 1;
END

SELECT
	 t.[idLine]  AS [idLine]
	,t.[idPoint] AS [idPoint]
	,t.[x]       AS [XValue]
	,t.[y]       AS [YValue]
FROM
	#t1 t
WHERE
	t.[idRecordSet] = 1
ORDER BY
	 t.[idLine] ASC
	,t.[idPoint] ASC
;

SELECT
	 t.[idLine]  AS [idLine]
	,t.[idPoint] AS [idPoint]
	,t.[x]       AS [XValue]
	,t.[y]       AS [YValue]
FROM
	#t1 t
WHERE
	t.[idRecordSet] = 2
ORDER BY
	 t.[idLine] ASC
	,t.[idPoint] ASC
;

SELECT
	 t.[idLine]  AS [idLine]
	,t.[idPoint] AS [idPoint]
	,t.[x]       AS [XValue]
	,t.[y]       AS [YValue]
FROM
	#t1 t
WHERE
	t.[idRecordSet] = 3
ORDER BY
	 t.[idLine] ASC
	,t.[idPoint] ASC
;
