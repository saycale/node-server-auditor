SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

DECLARE
	@intIndex [INTEGER]
;

CREATE TABLE #t1
(
	 [idRecordSet] [INTEGER]        NOT NULL
	,[x]           [DECIMAL](24, 4) NOT NULL
	,[y]           [DECIMAL](24, 4) NOT NULL
);

SET @intIndex = 1;

WHILE (@intIndex <= 5)
BEGIN
	INSERT INTO #t1
	(
		 [idRecordSet]
		,[x]
		,[y]
	)
	VALUES
	(
		 1
		,-5.0  + 10.0 * RAND()
		,-10.0 + 20.0 * RAND()
	);

	SET @intIndex = @intIndex + 1;
END

SET @intIndex = 1;

WHILE (@intIndex <= 10)
BEGIN
	INSERT INTO #t1
	(
		 [idRecordSet]
		,[x]
		,[y]
	)
	VALUES
	(
		 2
		,-50.0  + 100.0 * RAND()
		,-100.0 + 200.0 * RAND()
	);

	SET @intIndex = @intIndex + 1;
END

SET @intIndex = 1;

WHILE (@intIndex <= 15)
BEGIN
	INSERT INTO #t1
	(
		 [idRecordSet]
		,[x]
		,[y]
	)
	VALUES
	(
		 3
		,-500.0  + 100.0 * RAND()
		,-1000.0 + 200.0 * RAND()
	);

	SET @intIndex = @intIndex + 1;
END

SELECT
	 t.[x] AS [someVar1]
	,t.[y] AS [someVar2]
FROM
	#t1 t
WHERE
	t.[idRecordSet] = 1
ORDER BY
	t.[x] ASC
;

SELECT
	 t.[x] AS [someVar1]
	,t.[y] AS [someVar2]
FROM
	#t1 t
WHERE
	t.[idRecordSet] = 2
ORDER BY
	t.[x] ASC
;

SELECT
	 t.[x] AS [someVar1]
	,t.[y] AS [someVar2]
FROM
	#t1 t
WHERE
	t.[idRecordSet] = 3
ORDER BY
	t.[x] ASC
;
