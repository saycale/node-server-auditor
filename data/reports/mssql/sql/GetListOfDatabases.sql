SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

SELECT
	 @@ServerName                                                           AS [ServerName]
	,tSD.[dbid]                                                             AS [DatabaseId]
	,tSD.[name]                                                             AS [DatabaseName]
	,tSD.[crdate]                                                           AS [DatabaseCreationDate]
	,suser_sname(tSD.[sid])                                                 AS [DatabaseOwner]
	,CONVERT([NVARCHAR](128), DatabasePropertyEx(tSD.[name], N'Collation')) AS [DatabaseCollation]
FROM
	[master].[dbo].[sysdatabases] tSD
ORDER BY
	tSD.[name]
;
