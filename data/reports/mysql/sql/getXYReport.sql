SELECT
	 -10.53 + RAND() * 100 AS `someVar1`
	,-15.50 + RAND() * 100 AS `someVar2`

UNION ALL

SELECT
	 -0.57 + RAND() * 100 AS `someVar1`
	,+1.53 + RAND() * 100 AS `someVar2`

UNION ALL

SELECT
	 +5.51  + RAND() * 100 AS `someVar1`
	,+15.55 + RAND() * 100 AS `someVar2`

UNION ALL

SELECT
	 -15.51 + RAND() * 100 AS `someVar1`
	,-25.55 + RAND() * 100 AS `someVar2`

UNION ALL

SELECT
	 +25.31 + RAND() * 100 AS `someVar1`
	,+35.55 + RAND() * 100 AS `someVar2`
;
