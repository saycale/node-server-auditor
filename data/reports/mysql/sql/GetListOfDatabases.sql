SELECT
	 @@hostname                            AS `ServerName`
	,tIST.table_schema                     AS `DatabaseId`
	,tIST.table_schema                     AS `DatabaseName`
	,STR_TO_DATE('01/01/2016', '%m/%d/%Y') AS `DatabaseCreationDate`
	,'dba'                                 AS `DatabaseOwner`
	,'_'                                   AS `DatabaseCollation`
FROM
	information_schema.tables tIST
GROUP BY
	tIST.table_schema
;
