/* global angular */

/**
 * Graph Bar DataBase Component
 *
 * @requires BaseGraphComp
 * @requires Collection
 * @requires GraphBarDbData
 */
app.factory('GraphBarDbComp', ['BaseGraphComp', 'Collection', 'GraphBarDbData', function (BaseGraphComp, Collection, GraphBarDbData) {
	var GraphBarComp = function() {
		BaseGraphComp.call(this);

		angular.merge(this.options, {
			seriesDefaults: {
				renderer: $.jqplot.BarRenderer
			},
			axes: {
				xaxis: {
					renderer: $.jqplot.CategoryAxisRenderer,
					tickRenderer: $.jqplot.CanvasAxisTickRenderer,
					tickOptions: {
						labelPosition: 'middle',
						angle: -90
					}
				}
			},
			legend: {
				show:     true, // show legened
				location: 'ne', // compass direction, nw, n, ne, e, se, s, sw, w.
				xoffset:  12,   // pixel offset of the legend box from the x (or x2) axis.
				yoffset:  12    // pixel offset of the legend box from the y (or y2) axis.
			}
		});
	};

	angular.extend(GraphBarComp.prototype, BaseGraphComp.prototype, {
		updateReportData: function (data) {
			var graphLine = new Collection(GraphBarDbData, data);

			return [graphLine.data()];
		}
	});

	return GraphBarComp;
}]);
