app.factory('langLoader', ['$http', '$query', function ($http, $query) {
	// return loaderFn
	return function (options) {
		return $query('languages/' + options.key).then(function (data) {
			return data.response;
		});
	};
}]);
