app.service('alertify', ['$filter', function ($filter) {
	$.extend(true, $.noty.defaults, {
		layout: 'bottomRight',
		theme: 'relax',
		timeout: 5000,
		animation: {
			open: 'animated slideInRight',
			close: 'animated slideOutRight'
		}
	});

	return function (type, text) {
		noty( {
			text: $filter('translate')(text),
			type: type
		} );
	};
}]);
