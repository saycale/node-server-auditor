'use strict';

( function( module ) {
	var Sequelize    = require('sequelize');
	var database     = require('src/node/database')();
	var t_group      = require('./t_group');
	var t_connection = require('./t_connection');

	// table t_connection_group
	var TConnectionGroup = database.define( 't_connection_group', {
		// column 'id_connection'
		id_connection: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_connection,

				// This is the column name of the referenced model
				key: 'id_connection'
			}
		},

		// column 'id_group'
		id_group: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_group,

				// This is the column name of the referenced model
				key: 'id_group'
			}
		},

		// column 'is_active'
		is_active: {
			field:        'is_active',
			type:         Sequelize.BOOLEAN,
			allowNull:    false,
			comment:      'for soft delete of row',
			defaultValue: true
		}
	}, {
		// define the table's name
		tableName: 't_connection_group',

		comment: 'groups of connections',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TConnectionGroup.belongsTo(
				models.t_connection, {
					foreignKey: 'id_connection'
				}
			);

			TConnectionGroup.belongsTo(
				models.t_group, {
					foreignKey: 'id_group'
				}
			);
		},

		indexes: [ {
			name:   'uidx_t_connection_group',
			unique: true,
			fields: [
				'id_group',
				'id_connection'
			]
		} ]
	} );

	module.exports = TConnectionGroup;
} )( module );
