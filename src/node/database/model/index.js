'use strict';

(function( module ) {
	var tables = {
		t_connection:                 require( './t_connection' ),
		t_query_parameter:            require( './t_query_parameter' ),
		t_connection_type:            require( './t_connection_type' ),
		t_module:                     require( './t_module' ),
		t_query:                      require( './t_query' ),
		t_query_result_table:         require( './t_query_result_table' ),
		t_group:                      require( './t_group' ),
		t_connection_group:           require( './t_connection_group' ),
		t_group_query_schedule:       require( './t_group_query_schedule' ),
		t_report:                     require( './t_report' ),
		t_query_result_database:      require( './t_query_result_database' ),
		t_connection_query_parameter: require( './t_connection_query_parameter' ),
		t_query_location:             require( './t_query_location' ),
		t_connection_query_schedule:  require( './t_connection_query_schedule' )
	};

	for ( var foreignKeyTable in tables ) {
		if ( tables[foreignKeyTable].options.hasOwnProperty( 'associate' ) ) {
			tables[foreignKeyTable].options.associate(tables);
		}
	}

	module.exports = tables;
} )( module );
