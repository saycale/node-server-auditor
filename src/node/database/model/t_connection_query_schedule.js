'use strict';

(function( module ) {
	var Promise               = require( 'promise' );
	var Sequelize             = require( 'sequelize' );
	var _                     = require( 'lodash' );
	var main_storage_database = require( 'src/node/database' )();
	var t_report              = require( './t_report' );
	var t_module              = require( './t_module' );
	var t_group               = require( './t_group' );
	var t_connection          = require( './t_connection' );
	var t_connection_type     = require( './t_connection_type' );
	var logger                = require( 'src/node/log' );
	var nodeSchedule          = require( 'node-schedule' );

	var TConnectionQuerySchedule = main_storage_database.define( 't_connection_query_schedule', {
		// column 'id_connection_query_schedule'
		id_connection_query_schedule: {
			field:         'id_connection_query_schedule',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       'schedule identificator - primary key',
			validate:      {
			}
		},

		// column 'id_connection'
		id_connection: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_connection,

				// This is the column name of the referenced model
				key: 'id_connection'
			}
		},

		// column 'id_group'
		id_group: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_group,

				// This is the column name of the referenced model
				key: 'id_group'
			}
		},

		// column 'id_report'
		id_report: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_report,

				// This is the column name of the referenced model
				key: 'id_report'
			}
		},

		// column 'forGroup'
		forGroup: {
			field:        'forGroup',
			type:         Sequelize.BOOLEAN,
			allowNull:    false,
			defaultValue: false,
			comment:      'is execution schedule for the group of connections',
			validate:     {
			}
		},

		// column 'schedule'
		schedule: {
			field:     'schedule',
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   'execution schedule, like {30 * * * * *} - execute report every minute at 30 second',
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		},

		// column 'isEnabled'
		isEnabled: {
			field:        'isEnabled',
			type:         Sequelize.BOOLEAN,
			allowNull:    false,
			defaultValue: false,
			comment:      'is schedule enabled',
			validate:     {
			}
		}
	}, {
		// define the table's name
		tableName: 't_connection_query_schedule',

		comment: 'schedule query for the single server (connection)',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TConnectionQuerySchedule.belongsTo(
				models.t_report, {
					foreignKey: 'id_report'
				}
			);

			TConnectionQuerySchedule.belongsTo(
				models.t_connection, {
					foreignKey: 'id_connection'
				}
			);

			TConnectionQuerySchedule.belongsTo(
				models.t_group, {
					foreignKey: 'id_group'
				}
			);
		},

		indexes: [ {
			name:   'uidx_t_connection_query_schedule',
			unique: true,
			fields: [
				'id_connection',
				'id_group',
				'id_report',
				'forGroup',
				'schedule'
			]
		} ]
	} );

	module.exports = TConnectionQuerySchedule;
} )( module );
