'use strict';

(function( module ) {
	var Q                 = require( 'q' );
	var _                 = require( 'lodash' );
	var requireDir        = require( 'require-dir' );
	var path              = require( 'path' );
	var Promise           = require( 'promise' );
	var initials          = require( 'configuration' ).initials;
	var databases         = require( 'configuration' ).databases;
	var db                = require( 'src/node/database' )();
	var promiseWhile      = require( 'src/node/com/report/snapShot/promiseWhile' );
	var serverConfig      = require( 'src/node/com/report/serverConfiguration' );
	var logger            = require( 'src/node/log' );
	var jsonDBInitialize  = require('./json_initialization' );
	var nodeSchedule      = require( 'node-schedule' );
	var appDir            = path.dirname( require.main.filename );
	var modulesPath       = path.join( appDir, 'data' );
	var newconfig         = {};
	var reverseSupported  = {};
	var date              = new Date();

	function init() {
		// logger.debug( '[23]initial.js:init' );

		return db.sync()
			.then( jsonDBInitialize )
			.then( function() {
				// logger.debug( '[28]initial.js:init:fill table [t_connection_type]' );

				//
				// fill table 't_connection_type'
				//
				var bulkData = [];

				return db.models.t_connection_type.findAndCountAll().then( function( result ) {
					for ( var key in initials.supported ) {
						reverseSupported[key] = initials.supported[key].order;
					}

					if ( result.count == 0 ) {
						for ( var key in initials.supported ) {
							bulkData.push( {
								id_connection_type:   initials.supported[key].order,
								connection_type_name: key
							} );
						}

						return db.models.t_connection_type.bulkCreate( bulkData );
					} else {
						// logger.debug( '[50]initial.js:init' );

						return Q.resolve( reverseSupported );
					}
				}).then( function() {
					// logger.debug( '[55]initial.js:init:fill table [t_module]' );

					//
					// fill tables 't_module' and 't_report'
					//

					var reports  = requireDir( modulesPath );
					var bulkData = [];

					for ( var key in reports ) {
						var listReports = [];

						for ( var reportName in reports[key].reports ) {
							reports[key].reports[reportName].forEach(function( reportPath ) {
								var report = require( path.join( modulesPath, reportPath ) );

								listReports.push( {
									rep:     report,
									relPath: reportPath
								} );
							} );
						}

						bulkData.push( {
							module_name:        key,
							id_connection_type: reverseSupported[ reports[key].type ],
							reports:            listReports
						} );
					}

					// logger.debug( '[85]initial.js:init:fill table [t_module]' );

					return newconfig.saveModules( bulkData );
				}).then( function() {
					// logger.debug( '[89]initial.js:init:set newconfig' );

					newconfig.connectionType = initials.supported;
					newconfig.reverseType    = reverseSupported;

					return Q.resolve( reverseSupported );
				}).then( function( rev ) {
					// logger.debug( '[96]initial.js:init:fill table [t_group]' );

					//
					// fill table 't_group'
					//
					var serversData = serverConfig.getServersData();
					var servers     = serverConfig.getAvailable();
					var aGroupData  = [];
					var defer       = Q.defer();

					// logger.debug( '[106]serversData: {', serversData, '}' );

					// logger.debug( '[108]initial.js:init:fill table [t_group] with {No Group}' );

					//
					// TODO: code refactoring is required
					// TODO: we need to remove 'No Group' or setup 'No Group' for all availible connection types
					//

					// 1. set 'No Group' data
					aGroupData.push( {
						'id_group':           1,
						'id_connection_type': 1,
						'group_name':         'No Group',
						'is_active':          true
					} );
					aGroupData.push( {
						'id_group':           2,
						'id_connection_type': 2,
						'group_name':         'No Group',
						'is_active':          true
					} );

					// logger.debug( '[129]initial.js:init:fill table [t_group] with other groups' );

					// 2. find all connections and save groups
					for ( var i = 0; i < servers.length; i++ ) {
						if ( servers[i] in serversData ) {
							// logger.debug( '[134]servers[i]: {', servers[i], '}' );

							// check for unique group
							if ( 'groups' in serversData[servers[i]] ) {
								for ( var j = 0; j < serversData[servers[i]].groups.length; j++ ) {
									if ( findInArrayObjects( aGroupData, 'group_name', serversData[servers[i]].groups[j] ) == -1 ) {
										aGroupData.push( {
											'id_connection_type': reverseSupported[ serversData[servers[i]].type ],
											'group_name':         _.clone( serversData[servers[i]].groups[j] ),
											'is_active':          true
										} );
									}
								}
							}
						}
					}

					return db.transaction( function( t ){
						return db.models.t_group.update( 
							{
								is_active: 0
							},{
								where: {
									is_active: '1'
								}
							},{
								transaction: t
							}
						).then( function(){
							return Q.resolve();
						} );
					} ).then( function(){
						return newconfig.updateGroups( aGroupData );
					} );
				} ).then( function() {
					// logger.debug( '[170]initial.js:init:fill table [t_connection]' );

					//
					// fill table 't_connection'
					//
					var servers   = serverConfig.getAvailable();
					var aConnData = [];
					var aProm     = [];
					var defer     = Q.defer();
					var srvData   = null;
					var conn_type = '';
					var aPromVer  = [];

					return newconfig.updateConnectionActive()

					.then( function() {
						return db.transaction( function( tr ){
							for ( var i = 0; i < servers.length; i++ ){
								aProm.push( new Promise( function( resolve, reject ){
									var serverName = _.clone( servers[i] );

									srvData = serverConfig.getServersData( serverName );

									if ( srvData != null ){
										conn_type = _.clone( srvData.type );
									}

									return db.models.t_connection_type.findOrCreate( {
											where: {
												'connection_type_name': conn_type
											},
											transaction: tr
										} ).spread( function( TConnectionType, created ) {
											return db.models.t_connection.findOrCreate( {
												where: {
													'serverName':         serverName,
													'id_connection_type': TConnectionType.id_connection_type
												},
												defaults: {
													'serverName':         serverName,
													'id_connection_type': TConnectionType.id_connection_type,
													'serverVersion':      '0'
												},
												transaction: tr
											} ).spread( function( TConnection, created ){
												var innerD2 = Q.resolve();

												if( !created ) {
													TConnection.serverVersion = '0';

													innerD2 = TConnection.save();
												} else {
													resolve( TConnection.dataValues );

													return defer.resolve();
												}

												innerD2.then( function() {
													TConnection.is_active = true;

													innerD2 = TConnection.save();

													resolve( TConnection.dataValues );

													return defer.resolve();
												} );
											} );
										} );
								} ) );
							}

							return defer.promise;
						} ).then( function() {
							if ( aProm.length > 0 ) {
								return Promise.all( aProm ).then( function( aDataValues ) {
									return Q.resolve( aDataValues );
								},
								function( err ) {
									logger.error( '[237]', err );

									return Q.reject( err )
								} );
							}
						} );
					} );

				} )
				.then( function( aConnData ) {
					// logger.debug( '[247]initial.js:init' );

					return newconfig.saveGroupConnections( aConnData );
				} );
			} )
			.then( function(){
				return newconfig.saveResultDatabases();
			} )
			.then( function() {
				// logger.debug( '[256]Scheduler start' );

				startScheduller();
			} );
	};

	/*
	 * function to updates t_connection
	 * is_active to 0
	 */
	newconfig.updateConnectionActive = function(){
		return db.transaction( function( t ){
			return db.models.t_connection.update(
				{
					is_active: 0
				},{
					where: {
						is_active: '1'
					}
				},{
					transaction: t
				}
			).then( function(){
				return Q.resolve();
			} );
		} );
	};

	/*
	 * function to save modules data
	 *
	 */
	newconfig.saveModules = function( bulkData ) {
		// logger.debug( '[289]newconfig.saveModules' );

		return db.transaction( function( t ) {
			return db.models.t_module.update( {
					is_active: 0
				},{
					where: ['1']
				},{
					transaction: t
				}
			).then( function() {
				return Q.resolve();
			} );
		} ).then( function( res ) {
			var aQuery = [];

			return db.transaction( function( t ) {
				var index = 0;

				return promiseWhile( function() {
					return index < bulkData.length
				}, function() {
					var defer = Q.defer();

					// logger.debug( '[313]index: {', index, '}' );

					return db.models.t_module.findOrCreate( {
						where: {
							module_name:        bulkData[index].module_name,
							id_connection_type: bulkData[index].id_connection_type
						}
						//,transaction: t
					} ).spread( function( module, created ) {
						var innerD = Q.resolve();

						if ( !created ) {
							module.is_active = true;

							innerD = module.save();
						}

						return innerD.then( function() {
							aQuery.push( {
								id_module: _.clone( module.id_module ),
								supported: _.clone( initials.supported[module.id_connection_type] ),
								reports:   _.clone( bulkData[index].reports )
							} );

							index++;

							defer.resolve();
						} );
					} );

					return defer.promise;
				} );
			} ).then( function() {
				return db.transaction( function( t ) {
					var aProm = [];

					for( var i = 0; i < aQuery.length; i++ ) {
						aProm.push( new Promise( function( resolve, reject ) {
							return newconfig.saveQuery(
								_.clone( aQuery[i].id_module ),
								_.clone( aQuery[i].supported ),
								_.clone( aQuery[i].reports ),
								t
							).then( function() {
								resolve();

								return Q.resolve();
							} );
						} ) );
					}

					return Promise.all( aProm ).then( function() {
						return Q.resolve();
					} );
				} ).then( function() {
					return Q.resolve();
				} );
			} );
		} ).then( function() {
			return Q.resolve();
		} );
	};

	/*
	 * function to save reports data
	 *
	 */
	newconfig.saveQuery = function( id_module, id_connection_type, reports, trans ) {
		// logger.debug( '[381]newconfig.saveQuery' );
		// logger.debug( '[382]id_module: {', id_module, '}' );
		// logger.debug( '[383]id_connection_type: {', id_connection_type, '}' );
		// logger.debug( '[384]reports: {', reports, '}' );

		var index2 = 0;

		return promiseWhile( function() {
			return index2 < reports.length
		}, function() {
			var defer2  = Q.defer();
			var relpath = reports[index2].relPath.split( '/' );
			var aProm   = [];

			return db.models.t_report.findOrCreate( {
				where: {
					'id_module':       id_module,
					'report_name':     reports[index2].rep.name,
					'report_location': reports[index2].relPath
				},
				transaction: trans
			} ).spread( function( TReport, created ) {
				for ( var i = 0; i < reports[index2].rep.extract.length; i++ ) {
					for ( var j = 0; j < reports[index2].rep.extract[i].file.length; j++ ) {
						aProm.push( new Promise( function( resolve, reject ) {
							var inx  = _.clone( index2 );
							var file = _.clone( reports[inx].rep.extract[i].file[j] );

							return db.models.t_query.findOrCreate( {
								where: {
									'id_report': TReport.id_report,
								},
								transaction: trans
							} ).spread( function( TQuery, created ) {
								var innerD2 = Q.resolve();

								if ( !created ) {
									TQuery.is_active = true;

									innerD2 = TQuery.save();
								}

								innerD2.then( function() {
									return db.models.t_query_location.findOrCreate( {
										where: {
											'id_query':       TQuery.id_query,
											'version_min':    file.min,
											'version_max':    file.max,
											'query_location': file.file
										},
										transaction: trans
									} ).spread( function( TQueryLocation, created ) {
										resolve();

										return defer2.resolve();
									} );
								} );
							} );
						} ) );
					}
				}

				if ( aProm.length ) {
					relpath.pop();
					index2++;

					return Promise.all( aProm ).then( function( result ) {
						return defer2.promise;
					} );
				}
			} );

			relpath.pop();

			return defer2.promise;
		} );
	};

	/*
	 * function to update groups
	 *
	 */
	newconfig.updateGroups = function( aGroupData ) {
		var aProm  = [];
		var defer2 = Q.defer();

		// logger.debug( '[467]newconfig.updateGroups' );

		return db.transaction( function( t ) {
			for ( var i = 0; i < aGroupData.length; i++ ) {
				aProm.push( new Promise( function( resolve, reject ) {
					var rData = _.clone( aGroupData[i] );

					// logger.debug( '[474]rData: {', rData, '}' );

					return db.models.t_group.findOrCreate( {
						where: {
							group_name: rData.group_name
						},
						defaults: {
							group_name:         rData.group_name,
							id_connection_type: rData.id_connection_type,
							is_active:          true
						},
						transaction: t
					} ).spread( function( TGroup, created ) {
						var ret = Q.resolve();

						if ( !created ) {
							TGroup.is_active          = true;
							TGroup.id_connection_type = rData.id_connection_type;

							ret = TGroup.save();

							resolve();

							return defer2.resolve();
						} else {
							resolve();

							return defer2.resolve();
						}
					} );
				} ) );
			}

			return defer2.promise;
		} )
		.then( function() {
			if ( aProm.length > 0 ) {
				return Promise.all( aProm ).then( function( res ) {
					return defer2.resolve();
				},
				function( err ) {
					logger.error( '[513]', err );

					return defer2.reject( err );
				} );
			} else {
				return defer2.resolve();
			}
		},
		function( err ) {
			logger.error( '522', err );

			return defer2.reject( err );
		} );

		return Q.resolve();
	};

	/*
	 * function save connections
	 * and groups connections
	 */
	newconfig.saveGroupConnections = function( aConnData ) {
		var defer2 = Q.defer();
		var aProm  = [];

		// logger.debug( '[538]newconfig.saveGroupConnections' );

		if ( !aConnData ) {
			return Q.reject( {
				error: 'No Data Connection!'
			} );
		}

		return db.models.t_group.findAndCountAll().then( function( data ) {
			var srvData     = null;
			var aCreateData = [];

			if ( !data || !data.count ) {
				return defer2.reject();
			}

			for ( var i = 0; i < aConnData.length; i++ ) {
				srvData = serverConfig.getServersData( aConnData[i].serverName );

				// logger.debug( '[557]srvData: {', srvData, '}' );

				if ( srvData != null ) {
					if ( !( 'groups' in srvData ) ) {
						// logger.debug( '[561]aConnData[i].id_connection: {', aConnData[i].id_connection, '}' );

						aCreateData.push( {
							id_group:      1,
							id_connection: aConnData[i].id_connection
						} );
					} else {
						// find in all groups
						for ( var j = 0; j < data.count; j++ ) {
							if ( srvData.groups.indexOf( data.rows[j].group_name ) != -1 ) {
								// logger.debug( '[571]data.rows[j].id_group: {', data.rows[j].id_group, '}' );
								// logger.debug( '[572]aConnData[i].id_connection: {', aConnData[i].id_connection, '}' );

								aCreateData.push( {
									id_group:      data.rows[j].id_group,
									id_connection: aConnData[i].id_connection
								} );
							}
						}
					}
				}
			}

			return Q.resolve( aCreateData );
		} )
		.then( function( aData ) {
			// destroy data and create it
			return db.models.t_connection_group.destroy( {
				where: {
				}
			} )
			.then( function() {
				if ( aData.length ) {
					// logger.debug( '[594]main.sqlite initialize - Ok');

					return db.models.t_connection_group.bulkCreate( aData );
				} else {
					return defer2.resolve();
				}
			},
			function( err ) {
				logger.error( '[602]', err );

				return Q.reject( err );
			} );
		} );
	};

	/*
	 * function to save results
	 * databases in t_query_result_database
	 * Database names get from config.json
	 */
	newconfig.saveResultDatabases = function(){
		var d_keys = Object.keys( databases );
		var aProm  = [];
		var defer2 = Q.defer();

		// save to database
		return db.transaction( function( t ){
			for ( var i = 0; i < d_keys.length; i++ ) {
				aProm.push( new Promise( function( resolve, reject ) {
					return db.models.t_query_result_database.findOrCreate( {
						where: {
							result_database_name: d_keys[i]
						},
						transaction: t
					} ).spread( function( TQueryResultDatabase, created ) {
						resolve();
						return defer2.resolve();
					} )
				} ) );
			}
			return defer2.promise;
		} ).then( function() {
			if( aProm.length ) {
				return Promise.all( aProm ).then( function() {
					return Q.resolve();
				} );
			} else {
				return Q.resolve();
			}
		} );
	};

	/*
	 * function search value
	 * in array of objects
	 */
	function findInArrayObjects( arr, key, value ) {
		var ret = -1;

		if ( !arr || !key ) {
			return ret;
		}

		for ( var i = 0; i < arr.length; i++ ) {
			if ( key in arr[i] ) {
				if ( arr[i][key] == value ) {
					ret = i;

					break;
				}
			}
		}

		return ret;
	}

	/*
	 * function start Scheduller at the
	 * end of the initialization main.sqlite
	 */
	function startScheduller() {
		db.models.t_connection_query_schedule.sync( { force: false } ).then( function() {
			// logger.debug( '[676]startScheduller.sync' );

			var promises       = [];
			var Report         = require( 'src/node/com/report' )( 'start' );
			var activeSchedule = null;
			var jobName        = null;

			db.models.t_connection_query_schedule.findAll( {
				where: {
					isEnabled: true
				},
				include: [ {
					model: db.models.t_report,

					include: [ {
						model: db.models.t_module
					} ]
				}, {
					model: db.models.t_connection,

					include: [ {
						model: db.models.t_connection_type
					} ]
				}, {
					model: db.models.t_group,

					include: [ {
						model: db.models.t_connection_type
					} ]
				} ]
			} ).then( function( result ) {
				// logger.debug( '[707]result: {', result, '}' );

				result.forEach(function( item ) {
					// logger.debug( '[710]item: {', item, '}' );
					// logger.debug( '[711]item.isEnabled: {', item.isEnabled, '}' );

					if ( !item.isEnabled ) {
						return;
					}

					item.repName    = item.t_report.report_name;
					item.moduleType = item.t_report.t_module.module_name;
					item.connType   = item.t_connection.t_connection_type.connection_type_name;
					item.connName   = item.t_connection.serverName;

					// logger.debug( '[722]item.repName: {', item.repName, '}' );
					// logger.debug( '[723]item.moduleType: {', item.moduleType, '}' );
					// logger.debug( '[724]item.connType: {', item.connType, '}' );
					// logger.debug( '[725]item.connName: {', item.connName, '}' );

					// logger.debug( '[727]item.forGroup: {', item.forGroup, '}' );

					if ( !item.forGroup ) {
						promises.push( new Promise( function( resolve, reject ) {
							activeSchedule = nodeSchedule.scheduleJob( item.id_connection_query_schedule + '', item.schedule, function() {
								Report.getReportByScheduler( item.repName, item.connName, item.moduleType, true )
									.then(data => {
										return resolve( data );
									} )
									.catch( err => {
										logger.error( '[737]', err );

										return reject( err );
									} )
							} );
						} ) );
					} else {
						db.models.t_connection_group.findAndCountAll( {
							where: {
								id_group: item.id_group
							},
							include: [ {
								model: db.models.t_connection
							} ]
						} ).then( function( aData ) {
							if ( aData.count ) {
								for ( var i = 0; i < aData.count; i++ ) {
									promises.push( new Promise( function( resolve, reject ) {
										var connName2 = _.clone( aData.rows[i].dataValues.t_connection.dataValues.serverName );

										active_schedule = nodeSchedule.scheduleJob( item.id_connection_query_schedule + '', item.schedule, function() {
											Report.getReportByScheduler( item.repName, connName2, item.moduleType, true )
												.then( function( repData ) {
													resolve( repData );
												},
												function( err ) {
													logger.error( '[763]', err );

													reject( err );
												} );
											} );
									} ) );
								}
							} else {
								return dfd.reject();
							}
						} );
					}
				} );
			} )
			.catch( function( err ) {
				logger.error( '[778]', err );
				logger.error( '[779]startScheduller' );
			} )
			.finally( function() {
			} );
		} );
	}

	module.exports = {
		newconfig: newconfig,
		init:      init
	}
} )( module );
