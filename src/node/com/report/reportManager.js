'use strict';

(function() {
	var Q                   = require( 'q' );
	var extend              = require( 'extend' );
	var Promise             = require( 'promise' );
	var _                   = require( 'lodash' );
	var fs                  = require( 'fs' );
	var path                = require( 'path' );
	var Data                = require( 'src/node/com/data' );
	var ReportModuleFactory = require( 'src/node/com/report/reportModuleFactory' );
	var Connector           = require( 'src/node/com/report/connector/' );
	var SnapShot            = require( 'src/node/com/report/snapShot' );
	var Filters             = require( 'src/node/com/report/filters' );
	var Database            = require( 'src/node/database' );
	var ServerConfiguration = require( 'src/node/com/report/serverConfiguration' );
	var datamodels          = require( 'src/node/database/model' );
	var logger              = require( 'src/node/log' );
	var appDir              = path.dirname(require.main.filename);

	/**
	 * Report class
	 *
	 */
	function ReportManager() {
		/**
		 * configuration of the current report
		 */
		this.config = null;

		/**
		 * module of the current report
		 */
		this._module = null;

		/**
		 * connection of the current report
		 */
		this._server = null;

		/**
		 * module of the current report
		 */
		this._moduleFind = null;
	}

	/**
	 * Check if report is configured
	 *
	 * @returns {boolean}
	 */
	ReportManager.prototype.isConfigured = function() {
		return this.config instanceof Data.DB_Configuration;
	};

	/**
	 * Configure
	 *
	 * @param {Data.DB_Configuration} dbConf
	 * @returns {undefined}
	 */
	ReportManager.prototype.configure = function ( dbConf ) {
		if ( dbConf instanceof Data.DB_Configuration ) {
			this.config  = dbConf;
			this._module = null;

			return this.isConfigured();
		}

		if ( dbConf === null ) {
			this.config  = null;
			this._module = null;

			return true;
		}

		return false;
	};

	ReportManager.prototype.replaceParam11 = function( request, arrParam ) {
		for ( var i = 0; i < arrParam.length; i++ ) {
			request = request.replace( arrParam[i].name, arrParam[i].value );
		}

		return request;
	};

	ReportManager.prototype.jsonMenu = function() {
		if ( !this.isConfigured() ) {
			return false;
		}

		var module  = this.module();
		var repJson = module.get('repJson')

		return repJson;
	}

	/**
	 * Get list of all reports
	 *
	 * @returns {undefined}
	 */
	ReportManager.prototype.list = function() {
		if ( !this.isConfigured() ) {
			return false;
		}

		var result = [];
		var module = this.module();

		module.get( 'reports' ).map(function( report ) {
			result.push( report.get( 'name' ) );
		} );

		return result;
	};

	/**
	 * Get specified report
	 *
	 * @param {string} name
	 * @param {string} recent
	 * @param {string} connType
	 * @param {string} connName
	 * @param {string} moduleType
	 * @param {string} serverVersion
	 * @returns {object Q.Deferred}
	 */
	ReportManager.prototype.getReport = function( name, recent, connType, connName, moduleType, serverVersion ) {
		var aPrimaryKeys = [];
		var aPromReq     = [];
		var aStorage     = [];
		var connector    = null;
		var databaseName = '';
		var dfd          = Q(null);
		var loadStor     = '';
		var module       = null;
		var param_rep    = null;
		var prefs        = null;
		var queryShow    = null;
		var rPrimaryKeys = [];
		var report       = null;
		var reportName   = null;
		var request      = null;
		var request_test = null;
		var self         = this;
		var server       = null;

		// logger.debug( '[150]reportManager.js:getReport' );
		// logger.debug( '[151]name: {', name, '}' );
		// logger.debug( '[152]recent: {', recent, '}' );
		// logger.debug( '[153]connType: {', connType, '}' );
		// logger.debug( '[154]connName: {', connName, '}' );
		// logger.debug( '[155]moduleType: {', moduleType, '}' );
		// logger.debug( '[156]serverVersion: {', serverVersion, '}' );

		recent = recent || false;

		if ( moduleType ) {
			module = this.moduleFind( moduleType, serverVersion );
		} else {
			module = this.module();
		}

		if ( !module ) {
			logger.error( '[167]reportManager.js: report module is not defined' );
			logger.error( '[168]name: {', name, '}' );
			logger.error( '[169]recent: {', recent, '}' );
			logger.error( '[170]connType: {', connType, '}' );
			logger.error( '[171]connName: {', connName, '}' );
			logger.error( '[172]moduleType: {', moduleType, '}' );
			logger.error( '[173]serverVersion: {', serverVersion, '}' );

			return Q.reject( {
				error: 'report module is not defined'
			} );
		}

		if ( connType ) {
			if ( !Connector[connType] ) {
				logger.error( '[182]connector type not exists:connType: {', connType, '}' );

				dfd.reject( {
					error: 'connector type is not exists'
				} );

				return dfd.promise;
			}
		} else {
			if ( !Connector[module.get( 'type' )] ) {
				logger.error( '[192]reportManager.js: connector type is not defined' );
				logger.error( '[193]name: {', name, '}' );
				logger.error( '[194]recent: {', recent, '}' );
				logger.error( '[195]module: {', module, '}' );

				return Q.reject( {
					error: 'connector type is not exists'
				} );
			}
		}

		// logger.debug( '[199]name: {', name, '}' );

		report = module.get( 'reports' ).find( name );

		// logger.debug( '[203]name: {', name, '}' );
		// logger.debug( '[204]report: {', report, '}' );

		if ( !report ) {
			logger.error( '[211]reportManager.js: report not found' );
			logger.error( '[212]name: {', name, '}' );
			logger.error( '[213]recent: {', recent, '}' );
			logger.error( '[214]module: {', module, '}' );

			return Q.reject( {
				error: 'report(' + name + ') not found'
			} );
		}

		// logger.debug( '[217]connName: {', connName, '}' );

		if ( connName ) {
			server = this.server( connName );
		} else {
			server = this.config.get( 'server' );
		}

		// logger.debug( '[225]server: {', server, '}' );

		prefs = {};

		extend( true, prefs, server.get( 'prefs' ) );

		request_test = report.get( 'request' );
		queryShow    = report.get( 'queryShow' );
		reportName   = report.get( 'name' );
		loadStor     = report.get( 'storage' );

		// sort by order
		_.sortBy( request_test, 'order' );

		// set unique primary keys
		for ( var i = 0; i < request_test.length; i++ ) {
			rPrimaryKeys = request_test[i]['primary key'];

			if ( rPrimaryKeys ) {
				for ( var j = 0; j < rPrimaryKeys.length; j++ ) {
					if ( aPrimaryKeys.indexOf( rPrimaryKeys[j] ) == -1 ) {
						aPrimaryKeys.push( rPrimaryKeys[j] );
					}
				}
			}
		}

		if ( recent ) {
			queryShow = report.get( 'queryShow' );

			for ( var i = 0; i < request_test.length; i++ ) {
				request   = _.clone( request_test[i].query );
				param_rep = _.clone( request_test[i].report );

				// Add in array of promises
				aPromReq.push( new Promise( function( resolve, reject ) {
					var nQ             = _.clone( request );
					var repPrimaryKeys = _.clone( param_rep['primary key'] );
					var repSaveHistory = _.clone( param_rep.savehistoryrecords );
					var repStorage     = _.clone( param_rep.storage );
					var sectionName    = _.clone( param_rep.sectionName || '' );
					var params         = _.clone( request_test[i].parameters );
					var reportStorage  = null;

					if ( params ) {
						for ( var k = 0; k < params.length; k++ ) {
							params[k].paragraphName = 'extract';
							params[k].sectionName   = sectionName;
						}
					}

					resolve( dfd = self.loadParamsFromReport( name, module, server, nQ, params )
						.then( function( result ) {
							var tables;

							if ( result ) {
								if ( 'request' in result ) {
									request      = _.clone( result.request );
									databaseName = result.databaseName;

									if ( databaseName !== '' ) {
										prefs = replacePref( module.get('type'), prefs, databaseName );
									}
								}
							} else {
								request = _.clone( nQ );
							}

							if ( connType ) {
								connector = new Connector[connType]( prefs );
							} else {
								connector = new Connector[module.get('type')]( prefs );
							}

							return connector.query(request).then(function( tablesData ) {
								var notDrop = true;

								// if new storage, then destroy it first
								if ( aStorage.indexOf( repStorage ) == -1 ) {
									notDrop = false;

									aStorage.push( repStorage );
								}

								tables = Filters.validateDataSets( tablesData );

								reportStorage = Database( repStorage );

								var storePromise = SnapShot.set( reportStorage, {
									module:      module,
									name:        reportName,         // report.get('name'),
									primaryKeys: repPrimaryKeys,     // report.get('primaryKeys'),
									saveHistory: repSaveHistory,     // report.get('saveHistory'),
									serverName:  server.get( 'name' ),
									notDrop:     notDrop
									},
									tables
								);

								// require promise fulfil only in case of historical reports (which depends on the data saved)
								return queryShow ? storePromise : tables;
							} )
							.then( function() {
								return tables;
							}, function() {
								return tables;
							} )
						} )
						.catch( function( err ) {
							logger.error( '[345]', err );
							logger.error( '[346]reportManager.js' );

							return Q.reject( err );
						} )
					);
				} ) );
			}

			return Promise.all( aPromReq ).then( function( res ) {
				var rQ             = '';
				var aPromTrans     = [];
				var transform      = report.get( 'request_trans' );
				var loadStorage    = null;
				var params         = [];
				var snapShotParams = {
					module:      module,
					name:        reportName,        // report.get('name'),
					primaryKeys: aPrimaryKeys,      // report.get('primaryKeys'),
					serverName:  server.get( 'name' )
				};

				if ( !transform ) {
					transform = [];
				} else {
					_.sortBy( transform, ['order'] );
				}

				for ( var i = 0; i < transform.length; i++ ) {
					rQ     = _.clone( transform[i].query );
					params = _.clone( transform[i].parameters );

					if ( params ) {
						for ( var k = 0; k < params.length; k++ ) {
							params[k].paragraphName = 'transformation';
							params[k].sectionName   = transform[i].sectionName;
						}
					}

					aPromTrans.push( new Promise( function( resolve, reject ) {
						loadStorage = Database( transform[i].storage );

						resolve( self.loadParamsFromReport( name, module, server, rQ, params ).then( function( result ) {
							if ( result ) {
								if ( 'request' in result ) {
									request      = _.clone( result.request );
									databaseName = result.databaseName;

									if ( databaseName !== '' ) {
										prefs = replacePref( module.get('type'), prefs, databaseName );
									}
								}
							} else {
								request = _.clone( rQ );
							}

							resolve( SnapShot.transform( loadStorage, request, snapShotParams ) );
						} ) );
					} ) );
				}

				return Promise.all( aPromTrans ).then( function( results ) {
					return dfd.then( function( tables ) {
						var params         = null;
						var loadStorage    = Database( loadStor );
						var snapShotParams = {
							module:      module,
							name:        reportName,        // report.get('name'),
							primaryKeys: aPrimaryKeys,      // report.get('primaryKeys'),
							serverName:  server.get( 'name' )
						};

						if ( !queryShow ) {
							queryShow = report.get( 'queryShow' );
						}

						if ( queryShow ) {
							// first, get load params and modify query
							params = report.get( 'parameters' );

							if ( params ) {
								for ( var k = 0; k < params.length; k++ ) {
									params[k].paragraphName = 'load';
									params[k].sectionName   = 'load';
								}

								return self.loadParamsFromReport( name, module, server, queryShow, params ).then( function( result ) {
									if ( result ) {
										if ('request' in result) {
											request      = _.clone( result.request );
										}
									} else {
										request = _.clone( queryShow );
									}

									return SnapShot.queryShow( loadStorage, snapShotParams, request );
								} );
							} else {
								return SnapShot.queryShow( loadStorage, snapShotParams, queryShow );
							}
						}

						if ( tables ) {
							return tables;
						}

						return SnapShot.get( loadStorage, snapShotParams ).catch(function (err) {
							return [];
						} );
					} )
					.then( function ( dataSet ) {
						var data   = dataSet.slice(0);
						var result = new Data.ReportResult( report, data );

						return result.data();
					} )
					.catch( function( err ) {
						logger.error( '[455]', err );
						logger.error( '[456]reportManager.js' );

						var result = new Data.ReportResult(report, err);

						return Q.reject(result.data());
					} );
				} );
			},
			function( err ) {
				logger.error( '[465]', err );
				logger.error( '[466]reportManager.js' );

				return err;
			} );
		} else {
			return dfd.then( function( tables ) {
				var loadStorage    = Database( loadStor );
				var snapShotParams = {
					module:      module,
					name:        reportName,        // report.get('name'),
					primaryKeys: aPrimaryKeys,      // report.get('primaryKeys'),
					serverName:  server.get( 'name' )
				};

				if ( queryShow ) {
					return SnapShot.queryShow( loadStorage, snapShotParams, queryShow );
				}

				if ( tables ) {
					return tables;
				}

				return SnapShot.get( loadStorage, snapShotParams ).catch( function( err ) {
					return [];
				} );
			} )
			.then( function( dataSet ) {
				var data   = dataSet.slice(0);
				var result = new Data.ReportResult(report, data);

				return result.data();
			} )
			.catch( function( err ) {
				logger.error( '[499]', err );
				logger.error( '[500]reportManager.js:report: {', report, '}' );

				var result = new Data.ReportResult(report, err);

				return Q.reject( result.data() );
			} );
		}

		function replacePref( type, pref, valueDatabase ) {
			switch( type ) {
				case 'mssql':
				case 'mysql':
					if ( 'database' in pref ) {
						pref.database = valueDatabase;

						if ( 'options' in pref ) {
							pref.options.database = valueDatabase;
						}
					}
					break;
				default:
					logger.error( '[521]Unknow connection type: {', type, '}' );
					break;
			}

			return pref;
		}
	};

	ReportManager.prototype.module = function() {
		if ( this._module === null ) {
			if ( this.isConfigured() ) {
				this._module = ReportModuleFactory(this.config.get( 'module' ), this.config.get( 'version' ));
			} else {
				return false;
			}
		}

		return this._module;
	};

	/**
	 * Get specified report by scheduler
	 *
	 * @param {string} name
	 * @param {string} connType
	 * @param {string} connName
	 * @param {string} moduleType
	 * @param {string} recent
	 * @returns {object Q.Deferred}
	 */
	ReportManager.prototype.getReportByScheduler = function ( name, connName, moduleType, recent ) {
		// logger.debug( '[552]reportManager.js:ReportManager.prototype.getReportByScheduler' );
		// logger.debug( '[553]name: {', name, '}' );
		// logger.debug( '[554]connType: {', connType, '}' );
		// logger.debug( '[555]connName: {', connName, '}' );
		// logger.debug( '[556]moduleType: {', moduleType, '}' );
		// logger.debug( '[557]recent: {', recent, '}' );

		// start standart report with params
		var serverF = this.server( connName );
		var dfd     = Q.defer();
		var self    = this;

		// logger.debug( '[548]serverF: {', serverF, '}' );

		if ( !serverF ) {
			logger.error( '[567]reportManager.js:getReportByScheduler:Connection name is not defined' );

			dfd.reject( {
				error: 'Connection name is not defined'
			} );

			return dfd.promise;
		}

		dfd.resolve( datamodels.t_connection.findOne( {
			where: {
				'serverName': connName
			},
			include: [ {
				model: datamodels.t_connection_type
			} ]
		} ).then( function( rConnection ) {
			if ( rConnection ) {
				if( rConnection.serverVersion == '0' ) {
					// get version from server and update record
					ServerConfiguration.getServerVersion( serverF, rConnection.t_connection_type.connection_type_name, function( rVer ) {
						var innerD = Q.resolve();

						if( rVer && rVer.success ) {
							rConnection.serverVersion = rVer.ver;

							innerD = rConnection.save();

							innerD.then( function() {
								return self.getReport( name, recent, rConnection.t_connection_type.connection_type_name, connName, moduleType, rVer.ver );
							} );
						}
					} );
				} else {
					return self.getReport( name, recent, rConnection.t_connection_type.connection_type_name, connName, moduleType, rConnection.serverVersion );
				}
			}
		} ) );

		return dfd.promise;
	};

	ReportManager.prototype.getServer = function() {
		if ( this.isConfigured() ) {
			return this.config.get( 'server' );
		}

		return false;
	}

	/**
	 * Get version from config
	 *
	 * @return {string}
	 */
	ReportManager.prototype.getSavedVersion = function() {
		if ( this.isConfigured() ) {
			return this.config.get( 'version' );
		} else {
			return null;
		}
	};

	/**
	* Find module by name
	*
	* @param {string} name
	* @param {string} serverVer
	*/
	ReportManager.prototype.moduleFind = function ( name, serverVersion ) {
		// logger.debug( '[637]reportManager.js:moduleFind:name: {', name, '}' );
		// logger.debug( '[638]reportManager.js:moduleFind:name: {', serverVersion, '}' );

		var ver = ( serverVersion ) ? serverVersion : this.config.get( 'version' );

		// logger.debug( '[642]reportManager.js:moduleFind:ver: {', ver, '}' );

		if ( ver ) {
			this._moduleFind = ReportModuleFactory( name, ver );

			// logger.debug( '[647]reportManager.js:moduleFind:this._moduleFind: {', this._moduleFind, '}' );

			return this._moduleFind;
		} else {
			logger.error( '[651]reportManager.js:moduleFind:server version is unknown' );
			logger.error( '[652]name: {', name, '}' );

			return null;
		}
	};

	/**
	 * function get params select report
	 *
	 * nameRep
	 */
	ReportManager.prototype.loadParamsFromReport = function(nameRep, module, server, request, aParam) {
		var parameters = null;
		var aPromise   = [];
		var connType   = null;
		var connName   = null;
		var dfd        = Q.defer();
		var report     = null;

		if ( !module ) {
			return Q.reject( {
				error: 'no report module required'
			} );
		}

		connType = module.get( 'type' );
		connName = server.get( 'name' );
		report   = module.get( 'reports' ).find( nameRep );

		// outside params
		if ( aParam ) {
			parameters = aParam;
		} else {
			parameters = report.get( 'parameters' );
		}

		if ( !parameters ) {
			return Q(null);
		}

		parameters.forEach(function(param) {
			aPromise.push( new Promise( function( resolve, reject ) {
				datamodels.t_query_parameter.find( {
					where: {
						// connType:      connType,
						// connName:      connName,
						// repName:       nameRep,
						// TODO SSK check query params
						nameParam:     param.name,
						paragraphName: param.paragraphName || '',
						sectionName:   param.sectionName || ''
					}
				} ).then(function ( record ) {
					// if ( record ) {
					//    param.value = record.valueParam;
					// }

					resolve(param);
				});
			}));
		});

		Promise.all( aPromise ).then( function( resp ) {
			var jsonRow      = {};
			var resJson      = [];
			var nameParam    = '';
			var valueParam   = null;
			var databaseName = '';

			for ( var i = 0; i < resp.length; i++ ) {
				jsonRow = {};

				if ( resp[i].database === 'true' ) {
					nameParam    = 'noParam';
					databaseName = resp[i].value;
				} else {
					nameParam = resp[i].name;
				}

				try {
					switch(resp[i].type.toLowerCase()) {
						case 'string':
							valueParam = '\'' + resp[i].value + '\'';
							break;
						case 'integer':
							valueParam = parseInt(resp[i].value);
							break;
						case 'decimal':
							valueParam = parseFloat(resp[i].value);
							break;
						case 'date':
							valueParam = '\'' + (new Date(resp[i].value)).toDateString() + '\'';
							break;
						case 'datetime':
							valueParam = '\'' + (new Date(resp[i].value)).toString() + '\'';
							break;
						default:
							valueParam = '\'' + resp[i].value + '\'';
							break;
					}
				}
				catch ( err ) {
					valueParam = '\'' + resp[i].value + '\'';
				}

				request = request.replace(new RegExp(nameParam, 'g'), valueParam );
			}

			dfd.resolve( {
				'request':      request,
				'databaseName': databaseName
			} );
		}, function( err ) {
			logger.error( '[765]', err );

			dfd.reject(err);
		} );

		return dfd.promise;
	};

	/**
	* Find server by name
	*
	* @param {string} name
	*/
	ReportManager.prototype.server = function( name ) {
		// logger.debug( '[779]ReportManager.prototype.server' );
		// logger.debug( '[780]name: {', name, '}' );

		this._server = ServerConfiguration.getConfiguration( name );

		// logger.debug( '[784]this._server: {', this._server, '}' );

		return this._server;
	};

	module.exports = ReportManager;
} )();
